import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { databaseManager } from './utils/database/databaseManager';

const { SERVER_PORT } = process.env;

async function bootstrap() {
  databaseManager();
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(SERVER_PORT);
}

bootstrap();
