import { model, Schema, Model, Document } from 'mongoose';

export interface IRefreshToken extends Document {
  user_id: number;
  is_revoked: boolean;
  expires: Date;
}

const RefreshTokenSchema: Schema = new Schema({
  user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  is_revoked: { type: Boolean, required: true },
  expires: { type: Date, required: true },
});

const RefreshToken: Model<IRefreshToken> = model(
  'RefreshToken',
  RefreshTokenSchema,
);
export default RefreshToken;
