import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationController } from './authentication.controller';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { TokensModule } from '../tokens/tokens.module';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../../utils/constants/constants';
import { AuthenticationService } from './authentication.service';

describe('AuthenticationController', () => {
  let controller: AuthenticationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        UsersModule,
        PassportModule,
        TokensModule,
        JwtModule.register({
          secret: jwtConstants.JWT_ACCESS_TOKEN_SECRET,
          signOptions: {
            expiresIn: jwtConstants.JWT_ACCESS_TOKEN_EXPIRATION_TIME,
          },
        }),
      ],
      providers: [AuthenticationService],
      controllers: [AuthenticationController],
    }).compile();

    controller = module.get<AuthenticationController>(AuthenticationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
