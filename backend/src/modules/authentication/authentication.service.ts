import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User, UsersService } from '../users/users.service';
import { TokensService } from '../tokens/tokens.service';
import { RegisterRequest } from '../../requests/authentication';

export interface AuthenticationPayload {
  user: User;
  payload: {
    type: string;
    token: string;
    refresh_token?: string;
  };
}

@Injectable()
export class AuthenticationService {
  constructor(
    private usersService: UsersService,
    private tokensService: TokensService,
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    return this.usersService.verifyPassword(username, pass);
  }

  async login(username: string, password: string) {
    const valid = await this.validateUser(username, password);

    if (!valid) {
      throw new UnauthorizedException('The login is invalid');
    }
    const user = await this.usersService.findByUsername(username);

    const access_token = await this.tokensService.generateAccessToken(user);
    const refresh_token = await this.tokensService.generateRefreshToken(user);
    return AuthenticationService.buildResponsePayload(
      user,
      access_token,
      refresh_token,
    );
  }

  async register(user: RegisterRequest) {
    const payload = await this.usersService.create(user);

    const access_token = await this.tokensService.generateAccessToken(payload);
    const refresh_token = await this.tokensService.generateRefreshToken(
      payload,
    );
    return AuthenticationService.buildResponsePayload(
      payload,
      access_token,
      refresh_token,
    );
  }

  async refresh(refresh_token: string) {
    const { user, token } =
      await this.tokensService.createAccessTokenFromRefreshToken(refresh_token);

    return AuthenticationService.buildResponsePayload(user, token);
  }

  private static buildResponsePayload(
    user: User,
    accessToken: string,
    refreshToken?: string,
  ): AuthenticationPayload {
    return {
      user: { username: user.username, user_id: user.user_id || user._id },
      payload: {
        type: 'bearer',
        token: accessToken,
        ...(refreshToken ? { refresh_token: refreshToken } : {}),
      },
    };
  }
}
