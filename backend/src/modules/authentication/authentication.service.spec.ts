import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationService } from './authentication.service';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { TokensModule } from '../tokens/tokens.module';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../../utils/constants/constants';

describe('AuthenticationService', () => {
  let service: AuthenticationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        UsersModule,
        PassportModule,
        TokensModule,
        JwtModule.register({
          secret: jwtConstants.JWT_ACCESS_TOKEN_SECRET,
          signOptions: {
            expiresIn: jwtConstants.JWT_ACCESS_TOKEN_EXPIRATION_TIME,
          },
        }),
      ],
      providers: [AuthenticationService],
    }).compile();

    service = module.get<AuthenticationService>(AuthenticationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
