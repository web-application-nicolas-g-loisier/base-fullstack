import { Controller, Post, Body, BadRequestException } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import {
  LoginRequest,
  RefreshRequest,
  RegisterRequest,
} from '../../requests/authentication';

@Controller('api/auth/')
export class AuthenticationController {
  constructor(private authService: AuthenticationService) {}

  @Post('/register')
  async register(@Body() body: RegisterRequest) {
    console.log(body);
    if (!body.username || !body.password)
      throw new BadRequestException('Invalid register credential!');
    return this.authService.register(body);
  }

  @Post('/login')
  async login(@Body() body: LoginRequest) {
    if (!body.username || !body.password)
      throw new BadRequestException('Invalid login credential!');
    const { username, password } = body;
    return this.authService.login(username, password);
  }

  @Post('/refresh')
  public async refresh(@Body() body: RefreshRequest) {
    if (!body.refresh_token) throw new BadRequestException('No refresh token!');
    return this.authService.refresh(body.refresh_token);
  }
}
