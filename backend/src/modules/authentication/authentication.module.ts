import { Module } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { AuthenticationController } from './authentication.controller';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../../utils/constants/constants';
import { TokensModule } from '../tokens/tokens.module';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    TokensModule,
    JwtModule.register({
      secret: jwtConstants.JWT_ACCESS_TOKEN_SECRET,
      signOptions: { expiresIn: jwtConstants.JWT_ACCESS_TOKEN_EXPIRATION_TIME },
    }),
  ],
  providers: [AuthenticationService],
  controllers: [AuthenticationController],
})
export class AuthenticationModule {}
