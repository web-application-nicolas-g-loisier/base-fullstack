import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Injectable } from '@nestjs/common';

@Injectable()
@WebSocketGateway({ cors: true })
export class SocketsGateway
  implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit
{
  @WebSocketServer() private server: any;

  afterInit() {
    this.server.emit('testing', { do: 'stuff' });
  }

  handleConnection(client: any) {
    console.log('Connected =>', client.id);
  }

  handleDisconnect(client) {
    console.log('Disconnected =>', client.id);
  }

  broadcast(event: string, message: any) {
    console.log('Send =>', event);
    const broadCastMessage = JSON.stringify(message);
    this.server.emit(event, broadCastMessage);
  }

  @SubscribeMessage('my_event')
  myEvent(client: any, payload: any) {
    this.broadcast('my_event', payload);
  }
}
