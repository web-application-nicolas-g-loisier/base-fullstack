import { Injectable, BadRequestException } from '@nestjs/common';
import { UsersRepository } from './users.repository';
import { makePassword, verifyPassword } from '../../utils/hash/password';
import { UserDTO } from '../../dto/UserDTO';

export type User = any;

@Injectable()
export class UsersService {
  constructor(private usersRepository: UsersRepository) {}

  async verifyPassword(username: string, password: string): Promise<boolean> {
    const user = await this.usersRepository.getByUsernameWithPassword(username);

    return user && (await verifyPassword(password, user.password));
  }

  async create(user: any): Promise<UserDTO | undefined> {
    const existingFromUsername = await this.findByUsername(user.username);

    if (existingFromUsername) {
      throw new BadRequestException('Username already in use');
    }
    user.password = await makePassword(user.password);
    return await this.usersRepository.create(user);
  }

  async findByUsername(username: string): Promise<UserDTO | undefined> {
    return this.usersRepository.getByUsername(username);
  }

  async findById(id: any): Promise<UserDTO | undefined> {
    return this.usersRepository.getById(id);
  }

  async getAll(): Promise<Array<UserDTO>> {
    return this.usersRepository.getAll();
  }
}
