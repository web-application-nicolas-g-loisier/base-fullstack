import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersRepository } from './users.repository';
import { UsersController } from './users.controller';
import { JwtStrategy } from '../../utils/guards/jwt/jwt.strategy';
import { AdminStrategy } from '../../utils/guards/jwtAdmin/admin.strategy';

@Module({
  imports: [],
  providers: [UsersService, UsersRepository, JwtStrategy, AdminStrategy],
  exports: [UsersService],
  controllers: [UsersController],
})
export class UsersModule {}
