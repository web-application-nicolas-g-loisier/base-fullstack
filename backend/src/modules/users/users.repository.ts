import User, { IUser } from '../../models/user';
import { UserDTO } from '../../dto/UserDTO';
import { UserMapper } from '../../mapper/UserMapper';

export class UsersRepository {
  static returnSafeObject(user: any) {
    if (!user) return null;
    return {
      user_id: user._id,
      username: user.username,
      is_admin: user.is_admin || false,
    };
  }

  async create(user: any): Promise<UserDTO> {
    const new_user = await User.create(user);

    return UserMapper.modelToDTO(new_user);
  }

  async getAll(): Promise<Array<UserDTO>> {
    const users = await User.find();

    return users.map((user) => UserMapper.modelToDTO(user));
  }

  async getById(id: any): Promise<UserDTO> {
    const user = await User.findOne({ _id: id }).exec();

    return UserMapper.modelToDTO(user);
  }

  async getByUsername(username: string): Promise<UserDTO> {
    const user = await User.findOne({ username }).exec();

    if (!user) return undefined;

    return UserMapper.modelToDTO(user);
  }

  async getByUsernameWithPassword(username: string): Promise<IUser> {
    return await User.findOne({ username }).exec();
  }
}
