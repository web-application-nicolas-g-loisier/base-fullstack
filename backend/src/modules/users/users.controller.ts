import { Controller, Get, Param, Req, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../../utils/guards/jwt/jwt-auth.guard';
import { UsersService } from './users.service';
import { AdminAuthGuard } from '../../utils/guards/jwtAdmin/admin-auth.guard';

@Controller('api/users/')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @UseGuards(JwtAuthGuard)
  @Get('/me')
  async profile(@Req() request) {
    const userId = request.user.user_id;

    const user = await this.usersService.findById(userId);

    return {
      status: 'success',
      data: user,
    };
  }

  @UseGuards(AdminAuthGuard)
  @Get('/')
  async getAll() {
    const users = await this.usersService.getAll();

    return {
      status: 'success',
      data: users,
    };
  }

  @UseGuards(AdminAuthGuard)
  @Get('/:id')
  async getOne(@Param('id') id: string) {
    const user = await this.usersService.findById(id);

    return {
      status: 'success',
      data: user,
    };
  }
}
