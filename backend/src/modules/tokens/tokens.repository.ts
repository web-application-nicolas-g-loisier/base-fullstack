import { Injectable } from '@nestjs/common';
import { User } from '../users/users.service';
import RefreshToken, { IRefreshToken } from '../../models/refreshToken';
import { jwtConstants } from '../../utils/constants/constants';

@Injectable()
export class TokensRepository {
  public async createRefreshToken(user: User): Promise<IRefreshToken> {
    const token = new RefreshToken();

    token.user_id = user.user_id;
    token.is_revoked = false;

    const expiration = new Date();
    expiration.setTime(
      expiration.getTime() + jwtConstants.JWT_REFRESH_TOKEN_EXPIRATION_TIME,
    );

    token.expires = expiration;

    return token.save();
  }

  public async findTokenById(id: number): Promise<IRefreshToken | null> {
    return RefreshToken.findOne({
      _id: id,
    });
  }
}
