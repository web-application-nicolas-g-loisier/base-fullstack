import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { jwtConstants } from '../../utils/constants/constants';
import { TokensService } from './tokens.service';
import { TokensRepository } from './tokens.repository';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.JWT_ACCESS_TOKEN_SECRET,
      signOptions: { expiresIn: jwtConstants.JWT_ACCESS_TOKEN_EXPIRATION_TIME },
    }),
  ],
  providers: [TokensRepository, TokensService],
  exports: [TokensService],
})
export class TokensModule {}
