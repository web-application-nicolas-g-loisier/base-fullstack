import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { jwtConstants } from '../../utils/constants/constants';
import { JwtService } from '@nestjs/jwt';
import { TokensRepository } from './tokens.repository';
import { User, UsersService } from '../users/users.service';
import { TokenExpiredError } from 'jsonwebtoken';

@Injectable()
export class TokensService {
  constructor(
    private jwtService: JwtService,
    private tokenRepository: TokensRepository,
    private userService: UsersService,
  ) {}

  async generateAccessToken(user) {
    const opts = {
      subject: String(user.user_id),
    };

    const u = JSON.stringify(user);

    return this.jwtService.signAsync({ u }, opts);
  }

  async generateRefreshToken(user) {
    const token = await this.tokenRepository.createRefreshToken(user);

    const opts = {
      expiresIn: jwtConstants.JWT_REFRESH_TOKEN_EXPIRATION_TIME,
      subject: String(user.user_id),
      jwtid: String(token.id),
    };

    return this.jwtService.signAsync({}, opts);
  }

  public async resolveRefreshToken(
    encoded: string,
  ): Promise<{ user: User; token: any }> {
    const payload = await this.decodeRefreshToken(encoded);
    const token = await this.getStoredTokenFromRefreshTokenPayload(payload);

    if (!token) {
      throw new UnprocessableEntityException('Refresh token not found');
    }

    if (token.is_revoked) {
      throw new UnprocessableEntityException('Refresh token revoked');
    }

    const user = await this.getUserFromRefreshTokenPayload(payload);

    if (!user) {
      throw new UnprocessableEntityException('Refresh token malformed');
    }

    return { user, token };
  }

  public async createAccessTokenFromRefreshToken(
    refresh: string,
  ): Promise<{ token: string; user: User }> {
    const { user } = await this.resolveRefreshToken(refresh);

    const token = await this.generateAccessToken(user);

    return { user, token };
  }

  private async decodeRefreshToken(token: string): Promise<any> {
    try {
      return this.jwtService.verifyAsync(token);
    } catch (e) {
      if (e instanceof TokenExpiredError) {
        throw new UnprocessableEntityException('Refresh token expired');
      } else {
        throw new UnprocessableEntityException('Refresh token malformed');
      }
    }
  }

  private async getUserFromRefreshTokenPayload(payload: any): Promise<User> {
    const subId = payload.sub;

    if (!subId) {
      throw new UnprocessableEntityException('Refresh token malformed');
    }

    return this.userService.findById(subId);
  }

  private async getStoredTokenFromRefreshTokenPayload(
    payload: any,
  ): Promise<any | null> {
    const tokenId = payload.jti;

    if (!tokenId) {
      throw new UnprocessableEntityException('Refresh token malformed');
    }

    return this.tokenRepository.findTokenById(tokenId);
  }
}
