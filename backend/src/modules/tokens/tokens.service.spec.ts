import { Test, TestingModule } from '@nestjs/testing';
import { TokensService } from './tokens.service';
import { TokensRepository } from './tokens.repository';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../../utils/constants/constants';

describe('TokenService', () => {
  let service: TokensService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        UsersModule,
        PassportModule,
        JwtModule.register({
          secret: jwtConstants.JWT_ACCESS_TOKEN_SECRET,
          signOptions: {
            expiresIn: jwtConstants.JWT_ACCESS_TOKEN_EXPIRATION_TIME,
          },
        }),
      ],
      providers: [TokensRepository, TokensService],
    }).compile();

    service = module.get<TokensService>(TokensService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
