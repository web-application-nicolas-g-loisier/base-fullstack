export class LoginRequest {
  readonly username: string;
  readonly password: string;
}

export class RegisterRequest {
  readonly username: string;
  readonly password: string;
}

export class RefreshRequest {
  readonly refresh_token: string;
}
