import * as bcrypt from 'bcrypt';
import { bcryptConstants } from '../constants/constants';

export async function makePassword(plaintextPassword: string) {
  return bcrypt.hash(plaintextPassword, bcryptConstants.BCRYPT_SALT_ROUNDS);
}

export async function verifyPassword(
  plaintextPassword: string,
  hashedPassword: string,
) {
  return bcrypt.compare(plaintextPassword, hashedPassword);
}
