import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { jwtConstants } from '../../constants/constants';
import { UsersService } from '../../../modules/users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private userService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.JWT_ACCESS_TOKEN_SECRET,
    });
  }

  async validate(payload: any) {
    const { user_id } = payload;
    const user = this.userService.findById(user_id);

    if (!user)
      throw new HttpException('User not logged!', HttpStatus.UNAUTHORIZED);
    else return user;
  }
}
