import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { jwtConstants } from '../../constants/constants';
import { UsersService } from '../../../modules/users/users.service';

@Injectable()
export class AdminStrategy extends PassportStrategy(Strategy, 'admin') {
  constructor(private userService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.JWT_ACCESS_TOKEN_SECRET,
    });
  }

  async validate(payload: any) {
    const { user_id } = payload;
    const user = await this.userService.findById(user_id);

    if (!user) {
      throw new HttpException('User not logged!', HttpStatus.UNAUTHORIZED);
    } else if (!user.is_admin) {
      throw new HttpException('User not admin', HttpStatus.FORBIDDEN);
    } else return user;
  }
}
