import * as mongoose from 'mongoose';

const BASE_STR = '[Mongoose] ';
const { DB_HOST, DB_PORT, DB_NAME } = process.env;
const mongoURL = `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`;
const options = {};

export function databaseManager() {
  try {
    mongoose.connect(mongoURL, options, (error) => {
      if (error) {
        console.error(BASE_STR + 'Connection Error');
        console.error(error);
      } else {
        console.info(BASE_STR + 'Database connected');
      }
    });
  } catch (e) {
    console.error(BASE_STR + 'Connection Exception');
    console.error(e);
  }
}
