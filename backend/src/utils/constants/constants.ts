export const jwtConstants = {
  JWT_ACCESS_TOKEN_SECRET: 'secretKey',
  JWT_ACCESS_TOKEN_EXPIRATION_TIME: 3600,
  JWT_REFRESH_TOKEN_EXPIRATION_TIME: 2628000,
};

export const bcryptConstants = {
  BCRYPT_SALT_ROUNDS: 10,
};
