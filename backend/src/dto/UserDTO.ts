export class UserDTO {
  public user_id: string;
  public username: string;
  public is_admin: boolean;
}
