import { UserDTO } from '../dto/UserDTO';
import { IUser } from '../models/user';

export class UserMapper {
  static modelToDTO(userModel: IUser): UserDTO {
    const userDto = new UserDTO();

    userDto.user_id = userModel._id;
    userDto.username = userModel.username;
    userDto.is_admin = userModel.is_admin || false;

    return userDto;
  }
}
