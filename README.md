# BASE FULLSTACK

## Technology Used
- ReactJS
- Nest
- MongoDB
- Docker

## Getting Started
### Production (Docker)
```bash
$> docker-compose up --build -d
```

### Development (Local)
#### Installation
```bash
[frontend] $> npm install
[backend]  $> npm install
```
### Run
```bash
[frontend] $> npm run start
[backend]  $> npm run start:dev
```

## Environment Variables 

`DB_HOST`: Hostname of the database.<br/>
`DB_PORT`: Port of the database.<br/>
`DB_NAME`: Name of the database.<br/>
`DOCKER_DB_PORT`: Port of the database in the docker container.<br/>

`SERVER_HOST`: Hostname of the API.<br/>
`SERVER_PORT`: Port of the API.<br/>
`DOCKER_SERVER_PORT`: Port of API in the docker container.<br/>

## Test
### Frontend
### Backend