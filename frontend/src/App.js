import React from 'react';
import { ThemeProvider } from '@mui/styles';
import {
  Route,
  BrowserRouter as Router,
  Routes,
  Navigate,
} from 'react-router-dom';
import { createTheme, responsiveFontSizes } from '@mui/material/styles';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import { SocketContext, socket } from './context/socket';
import GlobalView from './views/GlobalView';
import HomeView from './views/HomeView/HomeView';
import LoginView from './views/LoginView/LoginView';

let theme = createTheme({
  palette: {
    background: {
      default: '#EAEAEA',
    },
  },
});
theme = responsiveFontSizes(theme);

function App() {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <ThemeProvider theme={theme}>
        <SocketContext.Provider value={socket}>
          <Router>
            <Routes>
              <Route path={'/'} element={<GlobalView />}>
                <Route index element={<HomeView />} />
              </Route>
              <Route path={'/login'} element={<LoginView />} />
              <Route path={'/register'} element={<LoginView />} />
              <Route path={'*'} element={<Navigate replace to="/" />} />
            </Routes>
          </Router>
        </SocketContext.Provider>
      </ThemeProvider>
    </LocalizationProvider>
  );
}

export default App;
