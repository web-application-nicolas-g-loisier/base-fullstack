import { createContext } from 'react';
import { io } from 'socket.io-client';

const serverPort = process.env.REACT_APP_SERVER_PORT || 8080;
const serverHost = process.env.REACT_APP_SERVER_HOSTNAME || 'localhost';

const SOCKET_URL = `http://${serverHost}:${serverPort}`;

export const socket = io(SOCKET_URL);
export const SocketContext = createContext();
