export async function serviceHelper(URL, content = {}) {
  URL = 'http://localhost:8080/api' + URL;

  return fetch(URL, content)
    .then((response) => {
      let funcToCall;
      switch (response.headers.get('content-type')) {
        case 'application/json;charset=UTF-8':
        case 'application/json; charset=utf-8':
        case 'application/json':
          funcToCall = 'json';
          break;
        case 'application/octet-stream':
          funcToCall = 'blob';
          break;
        default:
          funcToCall = 'text';
      }
      return response[funcToCall]().then((data) => ({ data, response }));
    })
    .then(({ data, response }) => {
      if (!response.ok) {
        return Promise.reject({ data: data, status: response.status });
      } else if (response.status >= 200 && response.status <= 299) {
        return data;
      }
      return Promise.reject({ data: data, status: response.status });
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}
