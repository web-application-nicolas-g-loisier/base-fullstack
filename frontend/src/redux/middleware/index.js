export default function middleware() {
  return function (next) {
    return function (action) {
      return next(action);
    };
  };
}
