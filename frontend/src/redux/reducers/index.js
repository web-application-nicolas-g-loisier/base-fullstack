import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import customisationReducer from './customisationReducer';
import snackbarReducer from './snackbarReducer';

const rootReducer = combineReducers({
  loginReducer,
  snackbarReducer,
  customisationReducer,
});

export default rootReducer;
