import {
  CUSTOMISATION_DARK_MODE,
  CUSTOMISATION_LANGUAGE,
} from '../constants/action-types';

export function toggleDarkMode(value) {
  return function (dispatch) {
    return dispatch({ type: CUSTOMISATION_DARK_MODE, payload: value });
  };
}

export function setLanguageIso2(value) {
  return function (dispatch) {
    return dispatch({ type: CUSTOMISATION_LANGUAGE, payload: value });
  };
}
