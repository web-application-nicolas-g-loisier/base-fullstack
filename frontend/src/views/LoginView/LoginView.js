import React, { useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { createStyles, makeStyles } from '@mui/styles';
import { Card } from '@mui/material';
import Login from '../../components/authentication/Login/Login';
import Register from '../../components/authentication/Register/Register';
import GlobalSnackbar from '../../components/global/GlobalSnackbar/GlobalSnackbar';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      backgroundColor: theme.palette.background.default,
      background:
        'url("https://climate.nasa.gov/system/news_items/main_images/2990_9827327865_98e0f0dc2d_o.jpg") no-repeat center center fixed',
      backgroundSize: 'cover',
      padding: '15vh 25vw',
      height: '70vh',
      [theme.breakpoints.down('md')]: {
        padding: '15vh 10vw',
        height: '70vh',
      },
    },
    card: {
      width: '100%',
      height: '100%',
      display: 'flex',
      border: '1px solid rgba(200, 200, 200, 1) !important',
      boxShadow: '15px 15px 10px rgba(0, 0, 0, 0.5) !important',
    },
    imgContainer: {
      width: '50%',
      backgroundColor: theme.palette.primary.dark,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      [theme.breakpoints.down('md')]: {
        width: 0,
      },
    },
    img: {
      width: 'auto',
      objectFit: 'fill',
    },
    info: {
      width: '100%',
      display: 'flex',
    },
  }),
);

function LoginView() {
  const classes = useStyles();
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const [value, setValue] = React.useState(pathname === '/login' ? 0 : 1);

  useEffect(() => {
    setValue(pathname === '/login' ? 0 : 1);
  }, [pathname, navigate]);

  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <div className={classes.info}>
          {value === 0 ? <Login /> : <Register />}
        </div>
      </Card>
      <GlobalSnackbar />
    </div>
  );
}

export default LoginView;
