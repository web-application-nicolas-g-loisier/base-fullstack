import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createStyles, makeStyles } from '@mui/styles';
import {
  Button,
  Checkbox,
  FormControlLabel,
  TextField,
  Typography,
} from '@mui/material';
import {
  validatePassword,
  validateUsername,
} from '../../../service/validator/authFormValidator';
import { snackbarError } from '../../../redux/actions/snackbarAction';
import { loginRequest } from '../../../service/serviceHelper/request/authentication';
import { login } from '../../../redux/actions/loginAction';

const useStyles = makeStyles(() =>
  createStyles({
    loginContainer: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      alignItems: 'center',
      width: '100%',
      margin: 'auto',
      gap: '20px',
    },
    loginHeader: {
      width: '80%',
    },
    textField: {
      width: '80%',
    },
    action: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '80%',
    },
    loginButton: {
      width: '80%',
    },
    register: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '80%',
    },
  }),
);

function Login({ snackbarError, login }) {
  const classes = useStyles();
  const navigate = useNavigate();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [usernameError, setUsernameError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);

  const loginClick = () => {
    const error1 = !validateUsername(username);
    setUsernameError(error1);
    const error2 = !validatePassword(password);
    setPasswordError(error2);

    if (error1) {
      snackbarError('Invalid Username');
    } else if (error2) {
      snackbarError('Invalid Password');
    } else {
      loginRequest(username, password)
        .then((res) => {
          navigate('/');
          login(res);
          //login(username, password)
        })
        .catch((err) => snackbarError(err.message));
    }
  };

  return (
    <div className={classes.loginContainer}>
      <div className={classes.loginHeader}>
        <Typography variant={'h3'} align={'left'}>
          Welcome Back!
        </Typography>
        <Typography variant={'subtitle1'} align={'left'}>
          Please enter your details.
        </Typography>
      </div>
      <TextField
        label="Username"
        variant="outlined"
        required
        value={username}
        onChange={(event) => setUsername(event.target.value)}
        error={usernameError}
        className={classes.textField}
      />
      <TextField
        label="Password"
        variant="outlined"
        required
        type={'password'}
        value={password}
        onChange={(event) => setPassword(event.target.value)}
        error={passwordError}
        className={classes.textField}
      />
      <div className={classes.action}>
        <FormControlLabel control={<Checkbox />} label="Remember me" />
        <Button>Forgot Password?</Button>
      </div>
      <Button
        variant={'contained'}
        onClick={loginClick}
        className={classes.loginButton}
      >
        Login
      </Button>
      <div className={classes.register}>
        <Typography>{"Doesn't have an account?"}</Typography>
        <Button href={'/register'}>Create Account</Button>
      </div>
    </div>
  );
}

Login.propTypes = {
  snackbarError: PropTypes.func.isRequired,
  login: PropTypes.func.isRequired,
};

export default connect(null, { snackbarError, login })(Login);
