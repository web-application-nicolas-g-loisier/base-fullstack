import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createStyles, makeStyles } from '@mui/styles';
import { Button, TextField, Typography } from '@mui/material';
import {
  validatePassword,
  validateUsername,
} from '../../../service/validator/authFormValidator';
import { snackbarError } from '../../../redux/actions/snackbarAction';
import { registerRequest } from '../../../service/serviceHelper/request/authentication';
import { register } from '../../../redux/actions/loginAction';

const useStyles = makeStyles(() =>
  createStyles({
    loginContainer: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      alignItems: 'center',
      width: '100%',
      margin: 'auto',
      gap: '20px',
    },
    loginHeader: {
      width: '80%',
    },
    textField: {
      width: '80%',
    },
    loginButton: {
      width: '80%',
    },
    login: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '80%',
    },
  }),
);

function Register({ snackbarError, register }) {
  const classes = useStyles();
  const navigate = useNavigate();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [usernameError, setUsernameError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [confirmPasswordError, setConfirmPasswordError] = useState(false);

  const registerClick = () => {
    const error1 = !validateUsername(username);
    setUsernameError(error1);
    const error2 = !validatePassword(password);
    setPasswordError(error2);

    if (error1) {
      snackbarError('Invalid Username');
    } else if (error2) {
      snackbarError('Invalid Password');
    } else if (password !== confirmPassword) {
      setConfirmPasswordError(true);
      snackbarError('Password and confirm password are different');
    } else {
      setConfirmPasswordError(false);
      registerRequest(username, password)
        .then((res) => {
          register(res);
          navigate('/');
        })
        .catch((err) => snackbarError(err.message));
    }
  };

  return (
    <div className={classes.loginContainer}>
      <div className={classes.loginHeader}>
        <Typography variant={'h3'} align={'left'}>
          Welcome!
        </Typography>
        <Typography variant={'subtitle1'} align={'left'}>
          Please enter your details.
        </Typography>
      </div>
      <TextField
        label="Username"
        variant="outlined"
        className={classes.textField}
        required
        value={username}
        onChange={(event) => setUsername(event.target.value)}
        error={usernameError}
      />
      <TextField
        label="Password"
        variant="outlined"
        className={classes.textField}
        type={'password'}
        required
        value={password}
        onChange={(event) => setPassword(event.target.value)}
        error={passwordError}
      />
      <TextField
        label="Confirm Password"
        variant="outlined"
        className={classes.textField}
        type={'password'}
        required
        value={confirmPassword}
        onChange={(event) => setConfirmPassword(event.target.value)}
        error={confirmPasswordError}
      />
      <Button
        variant={'contained'}
        className={classes.loginButton}
        onClick={registerClick}
      >
        Create Account
      </Button>
      <div className={classes.login}>
        <Typography>{'You have an account?'}</Typography>
        <Button href={'/login'}>Login</Button>
      </div>
    </div>
  );
}

Register.propTypes = {
  snackbarError: PropTypes.func,
  register: PropTypes.func,
};

export default connect(null, { snackbarError, register })(Register);
