import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { Divider, Drawer, List } from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import DrawerItem from './DrawerItem/DrawerItem';

const useStyles = makeStyles(() =>
  createStyles({
    drawer: {
      width: '250px',
    },
  }),
);

function mapStateToProps(state) {
  return {
    logged: state.loginReducer.logged,
  };
}

function GlobalDrawer({ toggleDrawer, open }) {
  const classes = useStyles();

  return (
    <Drawer
      anchor={'left'}
      open={open}
      onClose={toggleDrawer}
      className={classes.drawer}
    >
      <List className={classes.drawer}>
        <DrawerItem
          name={'Home'}
          link={'/'}
          icon={<FontAwesomeIcon icon={faHome} size="lg" />}
          toggleDrawer={toggleDrawer}
        />
        <Divider />
      </List>
    </Drawer>
  );
}

GlobalDrawer.propTypes = {
  open: PropTypes.bool,
  toggleDrawer: PropTypes.func,
};

export default connect(mapStateToProps, {})(GlobalDrawer);
