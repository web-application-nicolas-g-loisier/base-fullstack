import React from 'react';
import { ListItem, ListItemIcon, ListItemText } from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) =>
  createStyles({
    nested: {
      paddingLeft: theme.spacing(4),
    },
  }),
);

function DrawerItem({ icon, link, name, toggleDrawer, nested }) {
  const classes = useStyles();

  const handleClick = () => {
    if (window.location.pathname !== link) {
      window.location.replace(link);
    }
    toggleDrawer();
  };

  return (
    <ListItem
      button
      onClick={handleClick}
      className={nested ? classes.nested : null}
    >
      <ListItemIcon>{icon}</ListItemIcon>
      <ListItemText primary={name} />
    </ListItem>
  );
}

DrawerItem.propTypes = {
  nested: PropTypes.bool,
  toggleDrawer: PropTypes.func,
  link: PropTypes.string,
  icon: PropTypes.element,
  name: PropTypes.string,
};

export default DrawerItem;
