import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

function mapStateToProps(state) {
  return {
    logged: state.loginReducer.logged,
  };
}

function AdminRoute({ path, logged, children }) {
  return (
    <Route exact path={path}>
      {logged ? children : <Redirect to="/" />}
    </Route>
  );
}

AdminRoute.propTypes = {
  path: PropTypes.string,
  logged: PropTypes.bool,
  children: PropTypes.element,
};

export default connect(mapStateToProps, {})(AdminRoute);
